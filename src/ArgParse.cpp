// HentaiDl
// Copyright (C) 2018  Evrea
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>
#include <iostream>
#include <exception>

#include "ArgParse.hpp"

using std::vector;
using std::string;

ArgParse::ArgParse(int argc, char** argv)
{
    // Easier to work with vector than char**
    this->raw_arguments = vector<string>(argv + 1, argv + argc);
    this->parse_args();
}

void ArgParse::parse_args()
{
    for (auto arg: this->raw_arguments) {
        std::cout << arg << std::endl;
    }
    throw std::runtime_error("C murio :'v");
}
