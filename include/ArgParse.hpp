// HentaiDl
// Copyright (C) 2018  Evrea
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CLASS_ArgParse
#define CLASS_ArgParse

#include <vector>
#include <string>

class ArgParse
{
    public:
        ArgParse(int arg_count, char** arg_values);
        void add_flag(std::string name);

    private:
        void parse_args();
        std::vector<std::string> raw_arguments;
        std::vector<std::pair<bool, std::string>> flags;
};

#endif
